var express = require('express');
var path = require('path');
var router = express.Router();

// const User = require('../models/user');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {appName: 'NodeJS App 1'});
});

router.get('/users', function(req, res, next) {
  res.send('Hello Users.');
});

/* // Example POST route
router.post('/post-route', function(req, res, next) {
  let field1 = req.body.field1,
      field2 = req.body.field2;

  // process field1 and field2 here..
});
*/

/* // AUTH

// SESSION LOGIC
router.get('/', function(req, res, next) {
  if (req.session.isLoggedIn) {
    // the user is logged in
  } else {
    // the user is logged out
  }
});

// LOGIN ROUTE
router.post('/login', function(req, res, next) {
  if (req.body.username === 'user1' && req.body.password === '1234') {
    req.session.isLoggedIn = true;
    req.session.isUser = true;
    return res.redirect('/');
  } else {
    return res.redirect('/');
  }
});

// LOGOUT ROUTE
router.get('/logout', function(req, res, next) {
  req.session.destroy();  // destroy the current session;
  res.redirect('/');
});
*/

/*
    // FIND ALL USERS
    User.find({}, function(err, users) {
      if (err) next(err);

      res.render('users', {users: users});
    });

    // FIND USER MATCHING FILTER
    User.findOne({username: 'user1', password: '1234'}, (err, user) => {
      if (err) {
        // process error
      } else {
        if (user) {
          // process user
        }
      }
    });

    // CREATE USER
    User.create({
      firstName: 'James',
      lastName: 'Doe',
      username: 'james',
      password: '1234'
    }, (err, user) => {
      // if (err) next(err);

      // process 'user' here...
    });

    // Reference: http://mongoosejs.com/docs/api.html#Model
*/

module.exports = router;
