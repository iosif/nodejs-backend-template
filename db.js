const mongoose = require('mongoose');

const DB_URI = `mongodb://${process.env.DB_HOST}/${process.env.DB_NAME}`;

const db = {
    connect() {
        return mongoose.connect(DB_URI);
    },

    disconnect() {
        return mongoose.connection.close(() => {
            process.exit(0);
        });
    }
};

mongoose.Promise = global.Promise;

mongoose.connection.on('connected', () => {
    console.log('Mongoose connection open to ' + DB_URI);
});

mongoose.connection.on('error', (err) => {
    console.error(err);
});

process
  .on('SIGINT', db.disconnect)
  .on('SIGTERM', db.disconnect);

module.exports = db;